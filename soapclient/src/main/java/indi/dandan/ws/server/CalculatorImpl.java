package indi.dandan.ws.server;

import javax.jws.WebService;


/**
 *  这里WebService annotation里加了一个参数"endpointInterface"，这个参数用来指定这个WebService的抽象服务接口，
 *  例如此处如果不用"endpointInterface"指定接口，那么生成的WebService服务有三个操作"add","multi"和"minus"，也就是定义在当前类中的方法集；
 *  如果指定了endpointInterface，则只有"add","multi"，即定义在Calculator中的方法集。
 * 
 */
@WebService(endpointInterface = "indi.dandan.ws.server.Calculator")
public class CalculatorImpl implements Calculator {

    public int add(int a, int b) {
        return a+b;
    }

    public int multi(int a, int b) {
        return a*b;
    }

    public int minus(int a, int b) {  
        return a - b;  
    }  
}