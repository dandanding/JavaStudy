package org.jetty;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Server;


public class SimplestServer {

    public static void main(String[] args) throws Exception {
        Server server = new Server(8000);
        // TODO Add handler
        // 开启服务器
        server.start();
        server.join();
    }

}