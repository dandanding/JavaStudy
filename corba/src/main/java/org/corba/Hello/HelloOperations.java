package org.corba.Hello;


/**
* org/corba/Hello/HelloOperations.java .
* �� IDL-to-Java ������������ֲ�����汾 "3.2" ����
* ���� hello.idl
* 2017��7��25�� ���ڶ� ����08ʱ44��33�� CST
*/

public interface HelloOperations 
{
  String sayHello ();
  void shutdown ();
} // interface HelloOperations
