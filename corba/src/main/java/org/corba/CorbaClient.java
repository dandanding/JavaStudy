package org.corba;

import java.util.Properties;

import org.corba.Hello.Hello;
import org.corba.Hello.HelloHelper;
import org.omg.CosNaming.*;
import org.omg.CosNaming.NamingContextPackage.*;
import org.omg.CORBA.*;

public class CorbaClient {

	static Hello helloImpl;

	public static void main(String args[]) {
		try {

			Properties props = new Properties();
			// 初始化端口，默认的端口是1050
			props.put("org.omg.CORBA.ORBInitialPort", "1050");
			// 绑定到服务器
			props.put("org.omg.CORBA.ORBInitialHost", "127.0.0.1");

			// create and initialize the ORB
			ORB orb = ORB.init(args, props);

			// get the root naming context
			org.omg.CORBA.Object objRef = orb
					.resolve_initial_references("NameService");
			// Use NamingContextExt instead of NamingContext. This is
			// part of the Interoperable naming Service.
			NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

			// resolve the Object Reference in Naming
			String name = "Hello";
			helloImpl = HelloHelper.narrow(ncRef.resolve_str(name));

			System.out.println("Obtained a handle on server object: "
					+ helloImpl);
			System.out.println(helloImpl.sayHello());
			helloImpl.shutdown(); // 关闭

		} catch (Exception e) {
			System.out.println("ERROR : " + e);
			e.printStackTrace(System.out);
		}
	}

}