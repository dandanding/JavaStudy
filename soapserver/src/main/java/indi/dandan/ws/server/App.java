package indi.dandan.ws.server;

import javax.xml.ws.Endpoint;

public class App {

    public static void main(String[] args) {
        // 部署方式一：
        Endpoint.publish("http://localhost:8888/calculator", new CalculatorImpl());
        System.out.println("service publish!");
    }
}