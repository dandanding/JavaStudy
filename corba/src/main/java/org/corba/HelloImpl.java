package org.corba;

import org.corba.Hello.HelloPOA;
import org.omg.CORBA.ORB;

public class HelloImpl extends HelloPOA {

	private ORB orb;

	public void setORB(ORB orb_val) {
		this.orb = orb_val;
	}


	public void shutdown() {
		this.orb.shutdown(false);
	}

	public String sayHello() {
		// TODO Auto-generated method stub
		return "Hello";
	}
}
