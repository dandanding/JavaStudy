package indi.dandan.ws.client;

import indi.dandan.ws.server.Calculator;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;


public class App {
    public static void main(String[] args) throws Exception {
        QName qName = new QName("http://server.ws.dandan.indi/", "CalculatorImplService");
        URL url = new URL("http://localhost:8888/calculator?wsdl"); 
        Service service = Service.create(url, qName);
        Calculator calculator = service.getPort(Calculator.class); 
        System.out.println(calculator.add(10, 5));
    }

}