
## 参考资料   
		http://docs.oracle.com/javase/7/docs/technotes/guides/idl/jidlExample.html   

corba 程序开发步骤

## 1. 编写idl(interface define language)文件

## 2. 生成服务端客户端代码
		idlj -fall -pkgPrefix [word] [pkg] xx.idl

## 3. 继承xxPOA.java 编写实现代码

## 4. 服务端代码，将对象注册到orb上

## 5. 编写客户端代码，绑定 xxService 对象

## 6. 启动CorbaServer前， 先启动orbd   
		orbd -ORBInitialPort 1050  

## 7. 启动客户端发起调用    

##  其他的还有交易服务、事件服务等  