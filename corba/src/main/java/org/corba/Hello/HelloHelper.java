package org.corba.Hello;


/**
* org/corba/Hello/HelloHelper.java .
* �� IDL-to-Java ������������ֲ�����汾 "3.2" ����
* ���� hello.idl
* 2017��7��25�� ���ڶ� ����08ʱ44��33�� CST
*/

abstract public class HelloHelper
{
  private static String  _id = "IDL:Hello/Hello:1.0";

  public static void insert (org.omg.CORBA.Any a, org.corba.Hello.Hello that)
  {
    org.omg.CORBA.portable.OutputStream out = a.create_output_stream ();
    a.type (type ());
    write (out, that);
    a.read_value (out.create_input_stream (), type ());
  }

  public static org.corba.Hello.Hello extract (org.omg.CORBA.Any a)
  {
    return read (a.create_input_stream ());
  }

  private static org.omg.CORBA.TypeCode __typeCode = null;
  synchronized public static org.omg.CORBA.TypeCode type ()
  {
    if (__typeCode == null)
    {
      __typeCode = org.omg.CORBA.ORB.init ().create_interface_tc (org.corba.Hello.HelloHelper.id (), "Hello");
    }
    return __typeCode;
  }

  public static String id ()
  {
    return _id;
  }

  public static org.corba.Hello.Hello read (org.omg.CORBA.portable.InputStream istream)
  {
    return narrow (istream.read_Object (_HelloStub.class));
  }

  public static void write (org.omg.CORBA.portable.OutputStream ostream, org.corba.Hello.Hello value)
  {
    ostream.write_Object ((org.omg.CORBA.Object) value);
  }

  public static org.corba.Hello.Hello narrow (org.omg.CORBA.Object obj)
  {
    if (obj == null)
      return null;
    else if (obj instanceof org.corba.Hello.Hello)
      return (org.corba.Hello.Hello)obj;
    else if (!obj._is_a (id ()))
      throw new org.omg.CORBA.BAD_PARAM ();
    else
    {
      org.omg.CORBA.portable.Delegate delegate = ((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate ();
      org.corba.Hello._HelloStub stub = new org.corba.Hello._HelloStub ();
      stub._set_delegate(delegate);
      return stub;
    }
  }

  public static org.corba.Hello.Hello unchecked_narrow (org.omg.CORBA.Object obj)
  {
    if (obj == null)
      return null;
    else if (obj instanceof org.corba.Hello.Hello)
      return (org.corba.Hello.Hello)obj;
    else
    {
      org.omg.CORBA.portable.Delegate delegate = ((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate ();
      org.corba.Hello._HelloStub stub = new org.corba.Hello._HelloStub ();
      stub._set_delegate(delegate);
      return stub;
    }
  }

}
