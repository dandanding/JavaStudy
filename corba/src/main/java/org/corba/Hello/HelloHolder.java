package org.corba.Hello;

/**
* org/corba/Hello/HelloHolder.java .
* �� IDL-to-Java ������������ֲ�����汾 "3.2" ����
* ���� hello.idl
* 2017��7��25�� ���ڶ� ����08ʱ44��33�� CST
*/

public final class HelloHolder implements org.omg.CORBA.portable.Streamable
{
  public org.corba.Hello.Hello value = null;

  public HelloHolder ()
  {
  }

  public HelloHolder (org.corba.Hello.Hello initialValue)
  {
    value = initialValue;
  }

  public void _read (org.omg.CORBA.portable.InputStream i)
  {
    value = org.corba.Hello.HelloHelper.read (i);
  }

  public void _write (org.omg.CORBA.portable.OutputStream o)
  {
    org.corba.Hello.HelloHelper.write (o, value);
  }

  public org.omg.CORBA.TypeCode _type ()
  {
    return org.corba.Hello.HelloHelper.type ();
  }

}
